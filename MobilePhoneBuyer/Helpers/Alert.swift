//
//  Alert.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import UIKit
enum SortOption {
    case lowToHigh
    case highToLow
    case rating
    
    var title: String {
        switch self {
        case .lowToHigh:
            return "Price low to high"
        case .highToLow:
            return "Price high to low"
        case .rating:
            return "Rating"
        }
    }
    
}

class Alert {
    
    class func showSortOption(callBack: @escaping ((SortOption) -> Void)) {
        
        let lowToHighAction = UIAlertAction(title: SortOption.lowToHigh.title, style: .default) { (action) in
            callBack(.lowToHigh)
        }
        
        let highToLowAction = UIAlertAction(title: SortOption.highToLow.title, style: .default) { (action) in
            callBack(.highToLow)
        }
        
        let ratingAction = UIAlertAction(title: SortOption.rating.title, style: .default) { (action) in
            callBack(.rating)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: "Sort", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(lowToHighAction)
        alertController.addAction(highToLowAction)
        alertController.addAction(ratingAction)
        alertController.addAction(cancelAction)
        
        if let presented =  UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
            presented.present(alertController, animated: true, completion: nil)
        } else {
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
    class func showError() {
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        let alertController = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(cancelAction)
        
        if let presented =  UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
            presented.present(alertController, animated: true, completion: nil)
        } else {
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        
        
    }
    
}
