//
//  Product.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/9/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Product: Object, Mappable {
    
    @objc dynamic var brand: String = ""
    @objc dynamic var detail: String = ""
    @objc dynamic var thumbImageUrl: String?
    @objc dynamic var name: String = ""
    @objc dynamic var price: Float = 0.0
    @objc dynamic var id: Int = 0
    @objc dynamic var rating: Float = 0.0
    @objc dynamic var isFavorite: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        brand           <- map["brand"]
        detail          <- map["description"]
        thumbImageUrl   <- (map["thumbImageURL"])
        name            <- map["name"]
        price           <- map["price"]
        id              <- map["id"]
        rating          <- map["rating"]
    }
    
}
