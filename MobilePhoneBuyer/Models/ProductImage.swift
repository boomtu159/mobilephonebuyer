//
//  ProductDetail.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import ObjectMapper

class EscapedURLTransform : URLTransform {
    
    override func transformFromJSON(_ value: Any?) -> URL? {
        if let urlString = value as? String,
            let escaped = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            
            if escaped.hasPrefix("http://") ||  escaped.hasPrefix("https://") {
                return URL(string: escaped)
            } else {
                
                let urlString = String(format: "http://%@", escaped)
                return URL(string: urlString)
            }
        
        }
        return nil
    }
}

class ProductImage: Mappable {
    
    var productId: String?
    var url: URL?
    var id: String?
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        productId  <- map["mobile_id"]
        url        <- (map["url"], EscapedURLTransform())
        id         <- map["id"]
        
    }
    
}
