//
//  ProductDetailInteractor.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
protocol ProductDetailBusinessLogic {
    //Call when need to get product images
    func getProductImages(request: ProductDetail.FetchImage.Request)
}

protocol ProductDetailDataStore {
    var product: Product! { get set }
}

class ProductDetailInteractor: ProductDetailBusinessLogic, ProductDetailDataStore {
    
    var product: Product!
    var presenter: ProductDetailPresentationLogic?
    var worker: ProductDetailWorker = ProductDetailWorker(service: ProductDetailService())
    
    deinit {
        print("Product detail interactor free!!")
    }
    
    func getProductImages(request: ProductDetail.FetchImage.Request) {
        
        let id = String(format: "%d", request.id)
        
        //Display product detail first
        let response = ProductDetail.FetchImage.Response(productImages: [], product: product)
        presenter?.presentProductDetail(response: response)
        
        
        //Get all of product images
        worker.getProductImages(id: id) { [weak self] (result: ProductDetailResult<[ProductImage]>) in
            
            guard let weakSelf = self else { return }
            
            switch result {
            case .Success(let images):
                
                let urls = images.flatMap({ (productImage) -> URL? in
                    return productImage.url
                })
                
                let response = ProductDetail.FetchImage.Response(productImages: urls, product: weakSelf.product)
                weakSelf.presenter?.presentProductImage(response: response)
            case .Failure(let error):
                weakSelf.presenter?.presentError(error: error)
            }
            
        }
        
    }
}
