//
//  ProductDetailPresenter.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation

protocol ProductDetailPresentationLogic {
    ///Call when product detail ready and prepare viewModel for viewController
    func presentProductDetail(response: ProductDetail.FetchImage.Response)
    
    ///Call when product images ready and prepare viewModel for viewController
    func presentProductImage(response: ProductDetail.FetchImage.Response)
    
    ///Call when response ready and prepare viewModel for viewController
    func presentError(error: ProductDetailError)
}

class ProductDetailPresenter: ProductDetailPresentationLogic {
    
    weak var viewController: ProductDetailViewController?
    
    deinit {
        print("Product detail presenter free!!")
    }
    
    func presentProductDetail(response: ProductDetail.FetchImage.Response) {
        
        //Convert respons to viewModel
        let priceString = String(format: "$%.2f", response.product.price)
        let rating = String(format: "Rating: %.1f", response.product.rating)
        let detail = ProductDetail.FetchImage.ViewModel.DisplayedProductDetail(thumbnailUrls: response.productImages,
                                                                               name: response.product.name, description: response.product.detail, price: priceString, rating: rating)
        
        let viewModel = ProductDetail.FetchImage.ViewModel(displayedProduct: detail)
        viewController?.displayProductDetail(viewModel: viewModel)
    }
    
    func presentProductImage(response: ProductDetail.FetchImage.Response) {
        
        //Convert respons to viewModel
        let priceString = String(format: "$%.2f", response.product.price)
        let rating = String(format: "Rating: %.1f", response.product.rating)
        let detail = ProductDetail.FetchImage.ViewModel.DisplayedProductDetail(thumbnailUrls: response.productImages,
                                                                               name: response.product.name, description: response.product.detail, price: priceString, rating: rating)
        
        let viewModel = ProductDetail.FetchImage.ViewModel(displayedProduct: detail)
        viewController?.displayProductImage(viewModel: viewModel)
    }
    
    func presentError(error: ProductDetailError) {
        
    }
    
}
