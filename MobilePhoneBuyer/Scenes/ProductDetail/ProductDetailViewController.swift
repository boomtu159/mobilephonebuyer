//
//  ProductDetailViewController.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD

protocol PrductDetailDisplayLogic: class {
    ///Call when need to display product image
    func displayProductImage(viewModel: ProductDetail.FetchImage.ViewModel)
    
    ///Call when need to display product detail
    func displayProductDetail(viewModel: ProductDetail.FetchImage.ViewModel)

}

class ProductDetailViewController: UIViewController {

    var interactor: (ProductDetailBusinessLogic & ProductDetailDataStore)?
    var router: ProductDetailRouter?
    
    private var displayedProductDetail: ProductDetail.FetchImage.ViewModel.DisplayedProductDetail?
    
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var desctionLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    
    
    //Pager
    @IBOutlet private weak var pageControl: FSPageControl! {
        didSet {
            pageControl.setFillColor(.gray, for: .normal)
            pageControl.setFillColor(.red, for: .selected)
        }
    }
    @IBOutlet private weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(BannerPageCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.register(UINib(nibName: BannerPageCell.identifier, bundle: nil), forCellWithReuseIdentifier: BannerPageCell.identifier)
            self.pagerView.itemSize = .zero
            
        }
    }
    
    deinit {
        print("ProductDetail free!!")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
        setupNavigation()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
        setupNavigation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchImages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: Setup
    private func setup() {
        let viewController = self
        let interactor = ProductDetailInteractor()
        let presenter = ProductDetailPresenter()
        let router = ProductDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        
    }
    
    private func setupNavigation() {
        self.navigationItem.backBarButtonItem?.title = ""
    }
    
     //MARK: - Fetch Images
    private func fetchImages() {

        guard let product = interactor?.product else {  return }
        
        let request = ProductDetail.FetchImage.Request(id: product.id)
        interactor?.getProductImages(request: request)
    }

}

//MARK: - FSPagerView Datasource
extension ProductDetailViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        
        guard let detail = displayedProductDetail, let thumbnailUrls = detail.thumbnailUrls else {
            return 0
        }
        
        return thumbnailUrls.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        guard let detail = displayedProductDetail, let thumbnailUrls = detail.thumbnailUrls else {
            return FSPagerViewCell()
        }
        
        if let cell = pagerView.dequeueReusableCell(withReuseIdentifier: BannerPageCell.identifier, at: index) as? BannerPageCell {
            cell.setImage(with: thumbnailUrls[index])
            return cell
        }
        
        return FSPagerViewCell()
    }
    
    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
        return false
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
}

//MARK: - ProductDetailDisplayLogic
extension ProductDetailViewController: PrductDetailDisplayLogic {
    func displayProductImage(viewModel: ProductDetail.FetchImage.ViewModel) {
        displayedProductDetail = viewModel.displayedProduct
        
        pageControl.numberOfPages = viewModel.displayedProduct.thumbnailUrls?.count ?? 0
        pagerView.reloadData()
    }
    
    func displayProductDetail(viewModel: ProductDetail.FetchImage.ViewModel) {
        desctionLabel.text = viewModel.displayedProduct.description
        ratingLabel.text = viewModel.displayedProduct.rating
        priceLabel.text = viewModel.displayedProduct.price
        
        self.navigationItem.title = viewModel.displayedProduct.name
    }
}
