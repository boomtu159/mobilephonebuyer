//
//  BannerPageCell.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import UIKit
import Kingfisher
class BannerPageCell: FSPagerViewCell {

    @IBOutlet weak var bannerImageView: UIImageView!
    
    static let identifier = "BannerPageCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage(with url: URL?) {
        
        bannerImageView.contentMode = .scaleAspectFit
        
        bannerImageView.kf.setImage(with: url, placeholder: UIImage(named: "events-placeholder"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        
    }

}
