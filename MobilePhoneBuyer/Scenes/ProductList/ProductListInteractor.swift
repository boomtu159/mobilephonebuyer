//
//  ProductListInteractor.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/9/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import RealmSwift
protocol ProductListBusinessLogic {
    
    ///Call when need to fetch all product
    func fetchProducts(request: ProductList.FetchProducts.Request, needUpdate: Bool)
    
    ///Cal when need to sort product by option
    func sort(by option: SortOption, tabbarType: TabbarType)
    
    ///Call when need to set product to favorite list
    func setToFavorite(request: ProductList.SaveProduct.Request, tabbarType: TabbarType)
    
    ///Call when need to remove product from favorite list
    func removeFromFavorite(request: ProductList.SaveProduct.Request)
    
    ///Call when need to fetch favorite list
    func fetchFavoriteProducts(request: ProductList.FetchProducts.Request)
}

protocol ProductListDataStore {
    var products: [Product] { get }
    var favoriteProducts: [Product] { get }
    var currentTabbarType: TabbarType { get }
}

class ProductListInteractor: ProductListBusinessLogic, ProductListDataStore {
    
    ///Comunicate
    var presenter: ProductListPresentationLogic?
    var worker: ProductListWorker = ProductListWorker(service: ProductListService())
    
    ///DataStores
    var products: [Product] = []
    var favoriteProducts: [Product] = []
    var currentTabbarType: TabbarType = .all
    
    private var currentSortOption: SortOption = .lowToHigh
    
    deinit {
        print("Interactor free")
    }
    
    //MARK: - Fetch products
    func fetchProducts(request: ProductList.FetchProducts.Request, needUpdate: Bool) {
        
        currentTabbarType = .all
        
       
        if products.isEmpty || needUpdate {
            
             //If products is empty or need update just call api
            worker.fetchProducts { [weak self] (result: ProductListResult<[Product]>) in
                
                guard let weakSelf = self else { return }
                
                switch result {
                case .Success(let products):
                    
                    let response = ProductList.FetchProducts.Response(products: products)
                    weakSelf.presenter?.presentFetchedProducts(response: response)
                    weakSelf.products = products
                    weakSelf.sort(by: weakSelf.currentSortOption, tabbarType: weakSelf.currentTabbarType)
                    
                case .Failure(let error):
                    weakSelf.presenter?.presentError(error: error)
                }
                
            }
            
        } else {
            
            //If products existed just use it!
            let response = ProductList.FetchProducts.Response(products: products)
            presenter?.presentFetchedProducts(response: response)
        }
        
    }
    
    //MARK: - Sort products
    func sort(by option: SortOption, tabbarType: TabbarType) {
        
        currentSortOption = option
        currentTabbarType = tabbarType
        
        switch option {
        case .highToLow: 
            products = products.sorted(by: {$0.price > $1.price})
            favoriteProducts = favoriteProducts.sorted(by: {$0.price > $1.price})
        
        case .lowToHigh:
            products = products.sorted(by: {$0.price < $1.price})
            favoriteProducts = favoriteProducts.sorted(by: {$0.price < $1.price})

        case .rating:
            products = products.sorted(by: {$0.rating > $1.rating})
            favoriteProducts = favoriteProducts.sorted(by: {$0.rating > $1.rating})

        }
        
        var result: [Product] = []
        switch tabbarType {
        case .all:
            result = products
        case .favorite:
            result = favoriteProducts
        }
        
        let response = ProductList.FetchProducts.Response(products: result)
        self.presenter?.presentFetchedProducts(response: response)
        
        
    }
    
    //MARK: - Remove favorite
    func removeFromFavorite(request: ProductList.SaveProduct.Request) {
        
        let favoriteProduct = favoriteProducts[request.index]
        guard let index = products.index(where: {$0.id == favoriteProduct.id}) else { return }
        
        
        worker.removeFromFavorite(product: favoriteProduct) { [weak self] (result) in
            guard let weakSelf = self else { return }
            
            switch result {
            case .Success(let product):
                weakSelf.products[index] = product
                weakSelf.favoriteProducts.remove(at: request.index)

            case .Failure(let error):
                weakSelf.presenter?.presentFailToSaveAlert(error: error)
            }
            
        }
        
    }
    
    //MARK: - Set favorite
    func setToFavorite(request: ProductList.SaveProduct.Request, tabbarType: TabbarType) {
        
        let product = (tabbarType == .all) ? products[request.index] : favoriteProducts[request.index]
        
        if product.isFavorite {
    
            worker.removeFromFavorite(product: product) { [weak self] (result) in
                guard let weakSelf = self else { return }
                
                switch result {
                case .Success(let product):
                    weakSelf.products[request.index] = product
                    weakSelf.presenter?.presentToast(message: "Removed")
                case .Failure(let error):
                    weakSelf.presenter?.presentFailToSaveAlert(error: error)
                }

            }
        } else {
            worker.setToFavorite(product: product) { [weak self] (result) in
                guard let weakSelf = self else { return }
                switch result {
                case .Success(let product):
                    weakSelf.products[request.index] = product
                    weakSelf.presenter?.presentToast(message: "Favorited")
                case .Failure(let error):
                    weakSelf.presenter?.presentFailToSaveAlert(error: error)
                }
                
            }
        }
        
    }
    
    //MARK: - Fetch favorite list
    func fetchFavoriteProducts(request: ProductList.FetchProducts.Request) {
        
        currentTabbarType = .favorite
        
        worker.fetchFavorite { [weak self] (result: ProductListResult<[Product]>) in
            guard let weakSelf = self else { return }
            switch result {
            case .Success(let products):
                
                let response = ProductList.FetchProducts.Response(products: products)
                weakSelf.presenter?.presentFetchedProducts(response: response)
                weakSelf.favoriteProducts = products
                weakSelf.sort(by: weakSelf.currentSortOption, tabbarType: weakSelf.currentTabbarType)
                
            case .Failure(let error):
                weakSelf.presenter?.presentError(error: error)
            }
        }
    }
    
}
