//
//  ProductListPresenter.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/9/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation

protocol ProductListPresentationLogic {
    ///Call when response ready and prepare to display products
    func presentFetchedProducts(response: ProductList.FetchProducts.Response)
    
    ///Call when found something wrong
    func presentError(error: ProductListError)
    
    ///Call when need to show some toast
    func presentToast(message: String)
    
    ///Call when save favorite failed
    func presentFailToSaveAlert(error: ProductListError)
}

class ProductListPresenter: ProductListPresentationLogic {

    weak var viewController: ProductListDisplayLogic?
    
    func presentFetchedProducts(response: ProductList.FetchProducts.Response) {
        
        let products = response.products.flatMap { (product) -> ProductList.FetchProducts.ViewModel.DisplayedProduct in
            
            //Convert response to viewModel
            let priceString = String(format: "$%.2f", product.price)
            let rating = String(format: "Rating: %.1f", product.rating)
            let url = URL(string: product.thumbImageUrl ?? "")
            let displayedProduct = ProductList.FetchProducts.ViewModel.DisplayedProduct(thumbnailUrl: url,
                                                                                name: product.name,
                                                                                description: product.detail,
                                                                                price: priceString,
                                                                                rating: rating,
                                                                                isFavorite: product.isFavorite)
            return displayedProduct
            
        }
        
        let viewModel = ProductList.FetchProducts.ViewModel(displayedProducts: products)
        viewController?.displayFetchedProductList(viewModel: viewModel)
        
    }
    
    func presentToast(message: String) {
        //Triger to viewcontroller to show toast
        viewController?.displayToast(message: message)
    }
    
    func presentError(error: ProductListError) {
        //Triger to viewcontroller to show error
        viewController?.displayErrorAlert()
    }
    
    func presentFailToSaveAlert(error: ProductListError) {
        //Triger to viewcontroller to show error
        viewController?.displayErrorAlert()
    }
}
