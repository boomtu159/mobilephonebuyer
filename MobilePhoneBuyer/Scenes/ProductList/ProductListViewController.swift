//
//  ProductListViewController.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/9/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import UIKit
import MBProgressHUD
import PullToRefresh

enum TabbarType: Int {
    case all
    case favorite
    
}

protocol ProductListDisplayLogic: class {
    func displayFetchedProductList(viewModel: ProductList.FetchProducts.ViewModel)
    func displayFavoriteProductList(viewModel: ProductList.FetchProducts.ViewModel)
    func displayToast(message: String)
    func displayErrorAlert()
}

class ProductListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet private var tabbarButton: [UIButton]!
    
    var interactor: ProductListBusinessLogic?
    var router: ProductListRouter?

    private var displayedProducts: [ProductList.FetchProducts.ViewModel.DisplayedProduct] = []
    private var currentTabbarType: TabbarType = .all
    
    deinit {
        print("ViewController free!!")
        tableView.removeAllPullToRefresh()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupTableView()
        setupNavigation()
        fetchProducts(needUpdate: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Setups
    private func setup() {
        let viewController = self
        let interactor = ProductListInteractor()
        let presenter = ProductListPresenter()
        let router = ProductListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        
        tabbarButton[0].isSelected = true
        
    }
    
    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        
        let refresher = PullToRefresh()
        tableView.addPullToRefresh(refresher) { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.reloadData()
        }
        
    }
    
    private func setupNavigation() {
        
        let rightButton = UIButton(type: UIButtonType.custom)
        rightButton.setTitle("Sort", for: .normal)
        rightButton.addTarget(self, action: #selector(sortAction), for: UIControlEvents.touchUpInside)
        rightButton.setTitleColor(UIColor.blue, for: .normal)
        let rightBarButton = UIBarButtonItem(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
    }
    
    //MARK: - Navigate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router?.routeToShowProductDetail(segue: segue)
    }
    
    //MARK: - Fetch ProductList
    private func fetchProducts(needUpdate: Bool) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let request = ProductList.FetchProducts.Request()
        interactor?.fetchProducts(request: request, needUpdate: needUpdate)
    }
    
    private func fetchFavorite() {
        let request = ProductList.FetchProducts.Request()
        interactor?.fetchFavoriteProducts(request: request)
    }
    
    private func reloadData() {
        
        switch currentTabbarType {
        case .all:
            fetchProducts(needUpdate: true)
        case .favorite:
            fetchFavorite()
        }
        
    }
    
    //MARK: - Actions
    @objc func sortAction() {
        Alert.showSortOption { [weak self] (sortOption) in
            guard let weakSelf = self else { return }
            weakSelf.interactor?.sort(by: sortOption, tabbarType: weakSelf.currentTabbarType)
        }
    }
    @IBAction func tabbarAction(_ sender: Any) {
        
        guard let button = sender as? UIButton, let tabbarType = TabbarType(rawValue: button.tag)  else { return }
        
        currentTabbarType = tabbarType
        
        tabbarButton = tabbarButton.map({ (button) -> UIButton in
            button.isSelected = false
            return button
        })
        
        button.isSelected = true
        
        switch tabbarType {
        case .all:
            fetchProducts(needUpdate: false)
        case .favorite:
            fetchFavorite()
        }
        
        
    }
    
}

//MARK: - UITableView datasource & delegate
extension ProductListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayedProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductCell.cellIdentifier, for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        
        let viewModel = self.displayedProducts[indexPath.row]
        cell.setCell(with: viewModel, tabbarType: currentTabbarType)
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        switch currentTabbarType {
        case .all:
            return false
        case .favorite:
            return true
        
        }
        
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        switch currentTabbarType {
        case .all:
            return .none
        case .favorite:
            return .delete
            
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        switch editingStyle {
        case .delete:
            
            self.displayedProducts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        
            let request = ProductList.SaveProduct.Request(index: indexPath.row)
            interactor?.removeFromFavorite(request: request)
        
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

//MARK: - DisplayLogic
extension ProductListViewController: ProductListDisplayLogic {
    func displayFavoriteProductList(viewModel: ProductList.FetchProducts.ViewModel) {
        self.displayedProducts = viewModel.displayedProducts
        tableView.endAllRefreshing()

    }
    
    func displayFetchedProductList(viewModel: ProductList.FetchProducts.ViewModel) {
        
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.displayedProducts = viewModel.displayedProducts
        tableView.reloadData()
        tableView.endAllRefreshing()
    }
    
    func displayErrorAlert() {
        Alert.showError()
    }
    
    func displayToast(message: String) {
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .text
        hud.label.text = message
        hud.show(animated: true)
        hud.hide(animated: true, afterDelay: 0.5)
        
    }

}

//MARK: - ProductCell delegate
extension ProductListViewController: ProductCellDelegate {
    func favoriteDidTapped(index: Int) {
        
        let request = ProductList.SaveProduct.Request(index: index)
        self.interactor?.setToFavorite(request: request, tabbarType: currentTabbarType)
    }
}
