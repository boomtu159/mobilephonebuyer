//
//  ProductListWorker.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/9/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation


enum ProductListResult<T> {
    case Success(result: T)
    case Failure(error: ProductListError)
}

enum ProductListError: Equatable, Error {
    case ServerError
    case BadNetwork
    case CannotSave
    case CannotFetch
    case CannotDelete

}

class ProductListWorker {

    var service: ProductListServiceInterface!
    
    deinit {
        print("Worker👨🏼‍💻 free")
    }
    
    init(service: ProductListServiceInterface) {
        self.service = service
    }
    
    func fetchProducts(completionHandler: @escaping ProductListFetchCompletionHandler) {
        
        service.fetchProducts { (result: ProductListResult<[Product]>) in
            completionHandler(result)
        }
        
    }
    
    func setToFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        service.setToFavorite(product: product) { (result: ProductListResult<Product>) in
            completionHandler(result)
        }
    }
    
    func removeFromFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        service.removeFromFavorite(product: product) { (result: ProductListResult<Product>) in
            completionHandler(result)
        }
    }
    
    func fetchFavorite(completionHandler: @escaping ProductListFetchCompletionHandler) {
        service.fetchFavorite { (result: ProductListResult<[Product]>) in
            completionHandler(result)
        }
    }
    
    
}
