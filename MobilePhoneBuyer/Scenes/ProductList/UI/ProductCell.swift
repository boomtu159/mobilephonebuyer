//
//  ProductCell.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import UIKit
import Kingfisher

protocol ProductCellDelegate: class {
    func favoriteDidTapped(index: Int)
}

class ProductCell: UITableViewCell {

    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var thumbnalImageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var starButton: UIButton!
    
    weak var delegate: ProductCellDelegate?
    
    static let cellIdentifier = "ProductCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(with viewModel: ProductList.FetchProducts.ViewModel.DisplayedProduct, tabbarType: TabbarType) {
        
        productNameLabel.text = viewModel.name
        descriptionLabel.text = viewModel.description
        ratingLabel.text = viewModel.rating
        priceLabel.text = viewModel.price
        thumbnalImageView.kf.setImage(with: viewModel.thumbnailUrl, placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        starButton.isSelected = viewModel.isFavorite
        starButton.isHidden = (tabbarType == .favorite) ? true : false
        
    }
    
    //MARK: - Actions
    
    @IBAction func favoriteAction(_ sender: Any) {
        guard let button = sender as? UIButton else { return }
        button.isSelected = !button.isSelected
        delegate?.favoriteDidTapped(index: self.tag)
    }
    
}
