//
//  ProductService.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import RealmSwift
typealias ProductListFetchCompletionHandler = (ProductListResult<[Product]>) -> Void
typealias ProductListDBCompletionHandler = (ProductListResult<Product>) -> Void

protocol ProductListServiceInterface {
    func fetchProducts(completionHandler: @escaping ProductListFetchCompletionHandler)
    func setToFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler)
    func removeFromFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler)
    func fetchFavorite(completionHandler: @escaping ProductListFetchCompletionHandler)
}

class ProductListService: ProductListServiceInterface {
    
    private var realm: Realm?
    
    init() {
        
        do {
            realm = try Realm()
        } catch  {
            
        }
        
    }
    
    private func findProductListInFavorite(items: [Product], favorites: [Product]) -> [Product] {
        
        
        var result: [Product] = []
        items.forEach { (item) in
            
            let isFavorite = favorites.contains(where: { (product) -> Bool in
                return (product.id == item.id)
            })
            
            
            do {
                let realm = try Realm()
                try realm.write {
                    item.isFavorite = isFavorite
                }
            } catch {
                
            }
            
            result.append(item)
            
        }
        
        return result
        
    }
    
    func fetchProducts(completionHandler: @escaping ProductListFetchCompletionHandler) {
        let request = ProductListRequest()
        
        API.requestForItems(request: request) { [weak self] (items: [Product]?, error) in
            
            guard let weakSelf = self else { return }
            
            if let items = items {
                
                weakSelf.fetchFavorite(completionHandler: { (result) in
                    
                    switch result {
                    case .Success(let products):

                        //Find product that is favorited
                        let result = weakSelf.findProductListInFavorite(items: items, favorites: products)
                    
                        //Save all products to realm database
                        RealmDB.save(objects: result)

                        completionHandler(ProductListResult.Success(result: result))
                        
                    case .Failure(_):
                        completionHandler(ProductListResult.Failure(error: ProductListError.ServerError))

                    }
                    
                })
                
                
            } else {
                completionHandler(ProductListResult.Failure(error: ProductListError.ServerError))
            }
        }
    
    }
    
    func setToFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        do {
            
            let realm = try Realm()
            try realm.write {
                product.isFavorite = true
                realm.add(product, update: true)
            }
            completionHandler(ProductListResult.Success(result: product))
        } catch {
            completionHandler(ProductListResult.Failure(error: ProductListError.CannotSave))
        }
    }
    
    func removeFromFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        do {
            
            let realm = try Realm()
            try realm.write {
                product.isFavorite = false
                realm.add(product, update: true)
            }
            
            completionHandler(ProductListResult.Success(result: product))
        } catch {
            completionHandler(ProductListResult.Failure(error: ProductListError.CannotDelete))
        }
    }
    
    func fetchFavorite(completionHandler: @escaping ProductListFetchCompletionHandler) {
        
        do {
            if let products = try Product.get() as? [Product] {
                let filtered = products.filter({$0.isFavorite})
                let result = ProductListResult.Success(result: filtered)
                completionHandler(result)
            }
        } catch {
            completionHandler(ProductListResult.Failure(error: ProductListError.CannotFetch))
        }
        
    }
    
}

