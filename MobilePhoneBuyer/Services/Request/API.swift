//
//  API.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

class API {
    
    class func requestForItems<T: Mappable>(request: APIRequest, completionHandler: @escaping (_ item : [T]?, _ error : ErrorResponse?) -> Void) {
        let method = request.method
        let urlString = request.url
        let params = request.params
        let headers = request.headers
        
        Alamofire.request(urlString, method: method, parameters: params, headers: headers)
             .validate(statusCode: 200...1000)
             .responseArray(queue: nil, keyPath: request.responseKeyPath) { (response: DataResponse<[T]>) in
            
                if let value = response.result.value {
                    completionHandler(value, nil)
                } else {
                    do {
                        if let data = response.data {
                            let json:Any = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let errorResponse = Mapper<ErrorResponse>().map(JSONObject: json) {
                                completionHandler(nil, errorResponse)
                            }
                        }
                    } catch _ {
                        print("result - \(response)")
                        completionHandler(nil, nil)
                    }
                }
        }
        
    }
    
    class func requestForItem<T: Mappable>(request: APIRequest, completionHandler: @escaping (_ item : T?, _ error : ErrorResponse?) -> Void ) {
        
        let method = request.method
        let urlString = request.url
        let params = request.params
        let headers = request.headers
        
        Alamofire.request(urlString, method: method, parameters: params, headers: headers)
            .validate(statusCode: 200...1000)
            .responseObject(queue: nil, keyPath: request.responseKeyPath) { (response: DataResponse<T>) in
                
                if let value = response.result.value {
                    print("value - \(value)")
                    completionHandler(value, nil)
                } else {
                    do {
                        if let data = response.data {
                            let json:Any = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let errorResponse = Mapper<ErrorResponse>().map(JSONObject: json) {
                                completionHandler(nil, errorResponse)
                            }
                        }
                    } catch _ {
                        print("result - \(response)")
                        completionHandler(nil, nil)
                    }
                }
        }
        
    }
    
}

// MARK: - ErrorResponse
class ErrorResponse: Mappable {
    var status: String!
    var message: String!
    var data: String!
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.status       <- map["status"]
        self.message    <- map["message"]
        self.data       <- map["data"]
        
    }
}
