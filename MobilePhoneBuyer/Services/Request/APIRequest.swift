//
//  APIRequest.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import Alamofire

enum APIParameterType {
    case url
    case urlEncodedInUrl
    case json
}

class APIRequest {
    
    var method: Alamofire.HTTPMethod {
        return .get
    }
    
    
    var paramsType: APIParameterType {
        return .url
    }
    
    var url: String {
        return ""
    }
    
    var params: [String: Any]? {
        return nil
    }
    
    var responseKeyPath: String {
        return ""
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var shouldLog: Bool {
        return true
    }
}
