//
//  MockupProductListService.swift
//  MobilePhoneBuyerTests
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//


@testable import MobilePhoneBuyer
import XCTest

class MockupProductListSerivce: ProductListServiceInterface {
   
    var fetchProductListCalled = false
    var setToFavoriteCalled = false
    var removeFromFavoriteCalled = false
    var fetchFavoriteCalled = false
    
    func fetchProducts(completionHandler: @escaping ProductListFetchCompletionHandler) {
        
        fetchProductListCalled = true
        
        var products: [Product] = []
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = false
        
        products.append(product)
        
        completionHandler(ProductListResult.Success(result: products))
        
    }
    
    func setToFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        setToFavoriteCalled = true
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = true
        
        completionHandler(ProductListResult.Success(result: product))
        
    }
    
    func removeFromFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        removeFromFavoriteCalled = true
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = true
        
        completionHandler(ProductListResult.Success(result: product))

    }
    
    func fetchFavorite(completionHandler: @escaping ProductListFetchCompletionHandler) {
        fetchFavoriteCalled = true
        
        var products: [Product] = []
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = true
        
        products.append(product)
        
        completionHandler(ProductListResult.Success(result: products))
    }
    
    
    
    
}

