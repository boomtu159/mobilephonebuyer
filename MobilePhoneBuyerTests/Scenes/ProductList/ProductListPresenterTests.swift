//
//  ProductListPresenterTests.swift
//  MobilePhoneBuyerTests
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

@testable import MobilePhoneBuyer
import XCTest

//MARK: - Mockup ProductListDisplayLogic
class MockupProductListDisplayLogic: ProductListDisplayLogic {
    
    var displayFetchedProductListCalled = false
    var displayFavoriteProductListCalled = false
    var displaySaveFavoriteToastCalled = false
    var displayErrorAlertCalled = false

    var viewModel: ProductList.FetchProducts.ViewModel!

    
    func displayFetchedProductList(viewModel: ProductList.FetchProducts.ViewModel) {
        displayFetchedProductListCalled = true
        self.viewModel = viewModel
    }
    
    func displayFavoriteProductList(viewModel: ProductList.FetchProducts.ViewModel) {
        displayFavoriteProductListCalled = true
        self.viewModel = viewModel
    }
    
    func displayToast(message: String) {
        displaySaveFavoriteToastCalled = true
    }
    
    func displayErrorAlert() {
        displayErrorAlertCalled = true
    }
    
}

//MARK: - Tests
class ProductListPresenterTests: XCTestCase {
    
    var presenter: ProductListPresenter!
    static var products: [Product]!
    
    override func setUp() {
        super.setUp()
        
        setupPresenter()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //MARK: - Setup
    func setupPresenter() {
        
        presenter = ProductListPresenter()
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = true
        
        ProductListPresenterTests.products = [product]
        
    }
    
    func testPresentFetchedProductShouldShowProductCorrectly() {
        
        //Given
        let displayLogic = MockupProductListDisplayLogic()
        presenter.viewController = displayLogic
        
        //When
        let product = ProductListPresenterTests.products[0]
        let products = ProductListPresenterTests.products
        let response = ProductList.FetchProducts.Response(products: products!)
        presenter.viewController = displayLogic

        presenter.presentFetchedProducts(response: response)
        
        //Then
        XCTAssert(displayLogic.displayFetchedProductListCalled, "Presenting fetched products should trigger view controller to display them")
        XCTAssertEqual(product.brand, "Nokia", "Show correct product brand")
        XCTAssertEqual(product.detail, "Test detail", "Show correct product detail")
        XCTAssertEqual(product.name, "Nokia 6", "Show correct product name")
        XCTAssertEqual(product.thumbImageUrl, "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg", "Show correct product thumbnail")
        XCTAssertEqual(product.rating, 4.6, "Show correct product rating")
        XCTAssertEqual(product.price, 199.99, "Show correct product price")
        XCTAssertEqual(product.isFavorite, true, "Show correct product isFavorite")
        
    }
    
    func testPresentFetchedProductsShouldCallDisplayFetchedProductList () {
        
        //Given
        let displayLogic = MockupProductListDisplayLogic()
        presenter.viewController = displayLogic
        
        //When
        let products = ProductListPresenterTests.products
        let response = ProductList.FetchProducts.Response(products: products!)
        presenter.presentFetchedProducts(response: response)
        
        //Then
         XCTAssert(displayLogic.displayFetchedProductListCalled, "Presenting fetched products should trigger view controller to display them")
        
    }
    
    func testPresentErrorProductsShouldCallDisplayDisplayErrorAlert () {
        
        //Given
        let displayLogic = MockupProductListDisplayLogic()
        presenter.viewController = displayLogic
        
        //When
        presenter.presentError(error: ProductListError.ServerError)
        
        //Then
        XCTAssert(displayLogic.displayErrorAlertCalled, "In case failed from some something should trigger view controller to display Alert")
        
    }
    
    func testPresentToastShouldCallDisplaySaveFavoriteToast () {
        
        //Given
        let displayLogic = MockupProductListDisplayLogic()
        presenter.viewController = displayLogic
        
        //When
        presenter.presentToast(message: "Favorited")
        
        //Then
        XCTAssert(displayLogic.displaySaveFavoriteToastCalled, "Show toast should trigger view controller to display Toast with message")
        
    }
    
    func testFailToSaveAlertShouldCallDisplayErrorAlert () {
        
        //Given
        let displayLogic = MockupProductListDisplayLogic()
        presenter.viewController = displayLogic
        
        //When
        presenter.presentFailToSaveAlert(error: ProductListError.CannotSave)
        
        //Then
        XCTAssert(displayLogic.displayErrorAlertCalled, "Show save to favorite fail should trigger view controller to display error toast")
        
    }
    
}
