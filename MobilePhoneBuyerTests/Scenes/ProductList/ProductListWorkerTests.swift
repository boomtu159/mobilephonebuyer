//
//  ProductListWorkerTests.swift
//  MobilePhoneBuyerTests
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import XCTest
@testable import MobilePhoneBuyer

class ProductListWorkerTests: XCTestCase {
    
    var worker: ProductListWorker!
    static var products: [Product]!

    
    override func setUp() {
        super.setUp()
        
        setupWorker()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    //MARK: - Setup
    func setupWorker() {
        
        let service = MockupProductListSerivce()
        worker = ProductListWorker(service: service)
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = true
        
        ProductListWorkerTests.products = [product]
        
        
    }
    
    //MARK: - Tests
    func testFetchProductsShouldReturnListOfProducts() {
        
        //Given
        let service = worker.service as! MockupProductListSerivce
        
        // When
        var fetchedProducts = [Product]()
        let expect = expectation(description: "Wait for fetchProducts() to return")
        
        worker.fetchProducts { (result: ProductListResult<[Product]>) in
            
            switch result {
            case .Success(let products):
                fetchedProducts = products
            default: break
            }
            
            expect.fulfill()
            
        }
        
        waitForExpectations(timeout: 1.1)
        
        //Then
        XCTAssert(service.fetchProductListCalled, "Calling fetchProducts()")
        XCTAssertEqual(fetchedProducts.count, ProductListWorkerTests.products.count, "fetchProducts() should return a list of products")
        
        for product in fetchedProducts {
            XCTAssert(ProductListWorkerTests.products.contains(where: {$0.id == product.id}), "Fetched products should match the product in the data store")
        }
    }
    
    func testSetFavoriteShouldSaveSuccess() {
        //Given
         let service = worker.service as! MockupProductListSerivce
        let expect = expectation(description: "Wait for setToFavorite() to return")

        //When
        let product = ProductListWorkerTests.products[0]
        var isSuccess = false
        var savedProduct: Product?
        worker.setToFavorite(product: product) { (result) in
            
            switch result {
            case .Success(let product):
                isSuccess = true
                savedProduct = product
            case .Failure(_):
                isSuccess = false
            }
            
             expect.fulfill()
            
        }
        
        waitForExpectations(timeout: 1.1)
        
        //Then
        XCTAssert(service.setToFavoriteCalled, "Calling setToFavorite()")
        XCTAssertEqual(true, isSuccess, "Save success")
        XCTAssertEqual(product.id, savedProduct?.id, "Save right product to favorite list")
    }
    
    func testRemoveFromFavoriteShouldRemoveSuccess() {
        //Given
        let service = worker.service as! MockupProductListSerivce
        let expect = expectation(description: "Wait for removeFromFavorite() to return")
        
        //When
        let product = ProductListWorkerTests.products[0]
        var isSuccess = false
        var savedProduct: Product?
        worker.removeFromFavorite(product: product) { (result) in
            
            switch result {
            case .Success(let product):
                isSuccess = true
                savedProduct = product
            case .Failure(_):
                isSuccess = false
            }
            
            expect.fulfill()
            
        }
        
        waitForExpectations(timeout: 1.1)
        
        //Then
        XCTAssert(service.removeFromFavoriteCalled, "Calling removeFromFavorite()")
        XCTAssertEqual(true, isSuccess, "Remove success")
        XCTAssertEqual(product.id, savedProduct?.id, "Remove right product from favorite list")
    }
    
    func testFetchFavoritesShouldReturnListOfFavoriteProducts() {
        //Given
        let service = worker.service as! MockupProductListSerivce
        
        // When
        var fetchedProducts = [Product]()
        let expect = expectation(description: "Wait for fetchProducts() to return")
        
        worker.fetchFavorite { (result: ProductListResult<[Product]>) in
            
            switch result {
            case .Success(let products):
                fetchedProducts = products
            default: break
            }
            
            expect.fulfill()
            
        }
        
        waitForExpectations(timeout: 1.1)
        
        //Then
        XCTAssert(service.fetchFavoriteCalled, "Calling fetchFavorite()")
        XCTAssertEqual(fetchedProducts.count, ProductListWorkerTests.products.count, "fetchProducts() should return a list of products")
        
        for product in fetchedProducts {
            XCTAssertEqual(true, product.isFavorite, "Fetched favorite list isFavorite value should be true.")
            
        }
    }
    
    
    
}
